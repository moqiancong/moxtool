#!/bin/bash
me=$(whoami)
if [  "${me}" != "root" ] 
then
 echo "Will start installing with root access!"
 sudo $0
 exit 0
fi
echo "Starting to install skype for ubuntu system"
dpkg -s apt-transport-https > /dev/null || bash -c "sudo apt-get update; sudo apt-get install apt-transport-https -y"
echo "deb [arch=amd64] https://repo.skype.com/deb stable main" | sudo tee /etc/apt/sources.list.d/skypeforlinux.list
sudo apt-get update
sudo apt-get install skypeforlinux -y 
